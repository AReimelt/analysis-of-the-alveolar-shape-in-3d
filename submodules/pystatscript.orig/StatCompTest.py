#!/usr/bin/env python3
import sys
print(sys.version)
import csv
import glob
import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
from pandas import DataFrame
import scipy
from scipy import stats
import matplotlib.pyplot as plt
import numpy as np
import argparse

# Code for printing to a file 
StatCompTest = open('StatCompTest.txt', 'w')



options = argparse.ArgumentParser()
options.add_argument("-f1", "--file1", required=True)
options.add_argument("-f2", "--file2", required=True)
options.add_argument("-f3", "--file3", required=True)
options.add_argument("-f4", "--file4", required=True)
options.add_argument("-f5", "--file5", required=True)
options.add_argument("-f6", "--file6", required=True)
options.add_argument("-d1", "--direct1", required=True)
options.add_argument("-d2", "--direct2", required=True)
options.add_argument("-d3", "--direct3", required=True)
options.add_argument("-d4", "--direct4", required=True)
options.add_argument("-d5", "--direct5", required=True)
options.add_argument("-d6", "--direct6", required=True)
options.add_argument("-g1", "--filess1", required=True)
options.add_argument("-g2", "--filess2", required=True)
options.add_argument("-g3", "--filess3", required=True)
options.add_argument("-g4", "--filess4", required=True)
options.add_argument("-g5", "--filess5", required=True)
options.add_argument("-g6", "--filess6", required=True)
options.add_argument("-n1", "--neighbours1", required=True)
options.add_argument("-n2", "--neighbours2", required=True)
options.add_argument("-n3", "--neighbours3", required=True)
options.add_argument("-n4", "--neighbours4", required=True)
options.add_argument("-n5", "--neighbours5", required=True)
options.add_argument("-n6", "--neighbours6", required=True)
args = options.parse_args()
df1 = pd.read_csv(args.file1)
df2 = pd.read_csv(args.file2)
df3 = pd.read_csv(args.file3)
df4 = pd.read_csv(args.file4)
df5 = pd.read_csv(args.file5)
df6 = pd.read_csv(args.file6)


dfList = [df1, df2, df3, df4, df5, df6]

for df in dfList:
    mean1 = df['interplanarAngles'].mean()
    sum1 = df['interplanarAngles'].sum()
    max1 = df['interplanarAngles'].max()
    min1 = df['interplanarAngles'].min()
    count1 = df['interplanarAngles'].count()
    median1 = df['interplanarAngles'].median() 
    std1 = df['interplanarAngles'].std() 
    var1 = df['interplanarAngles'].var()
    
    print ('Mean interplanarAngles: ' + str(mean1), file = StatCompTest)
    print ('Sum of interplanarAngles: ' + str(sum1), file = StatCompTest)
    print ('Max interplanarAngles: ' + str(max1), file = StatCompTest)
    print ('Min interplanarAngles: ' + str(min1), file = StatCompTest)
    print ('Count of interplanarAngles: ' + str(count1), file = StatCompTest)
    print ('Median interplanarAngles: ' + str(median1), file = StatCompTest)
    print ('Std of interplanarAngles: ' + str(std1), file = StatCompTest)
    print ('Var of interplanarAngles: ' + str(var1), file = StatCompTest)



#The next section has the goal to analyze the relative and absolute facet sizes
dir1 = (args.direct1)
dir2 = (args.direct2)
dir3 = (args.direct3)
dir4 = (args.direct4)
dir5 = (args.direct5)
dir6 = (args.direct6)


dirList = [dir1, dir2, dir3, dir4, dir5, dir6]
print(dirList)

for dir in dirList:

    files2 = glob.glob(dir)

    file_list = []
    file_list2 = []
    file_list3 = []

    for f in files2:
        df3 = pd.read_csv(f)
        FacetSize = pd.DataFrame(df3['absFacetSize'])
        FacetSizec = FacetSize.dropna()
        FacetSizei = FacetSizec.index
        FacetSizen = len(FacetSizei)
        file_list2.append(FacetSizen)
        for j in range (0, FacetSizen):
            file_list.append(float(FacetSizec.iloc[j]))#float!!!


    dfObj = pd.DataFrame(file_list)

                      
    mean2 = dfObj.mean()
    sum2 = dfObj.sum()
    max2 = dfObj.max()
    min2 = dfObj.min()
    count2 = dfObj.count()
    median2 = dfObj.median() 
    std2 = dfObj.std() 
    var2 = dfObj.var()

    print ('Mean Facet Size' + str(mean2), file = StatCompTest)
    print ('Sum of Facet Sizes' + str(sum2), file = StatCompTest)
    print ('Max Facet Size' + str(max2), file = StatCompTest)
    print ('Min Facet Size' + str(min2), file = StatCompTest)
    print ('Count of Facet Sizes' + str(count2), file = StatCompTest)
    print ('Median of Facet Sizes' + str(median2), file = StatCompTest)
    print ('Std of Facet Size' + str(std2), file = StatCompTest)
    print ('Var of Facet Size' + str(var2), file = StatCompTest)

    dfFS = pd.DataFrame(file_list2)

    mean3 = dfFS.mean()
    sum3 = dfFS.sum()
    max3 = dfFS.max()
    min3 = dfFS.min()
    count3 = dfFS.count()
    median3 = dfFS.median() 
    std3 = dfFS.std() 
    var3 = dfFS.var()

    print ('Mean Facets' + str(mean3), file = StatCompTest)
    print ('Sum of Facets' + str(sum3), file = StatCompTest)
    print ('Max Facets' + str(max3), file = StatCompTest)
    print ('Min Facets' + str(min3), file = StatCompTest)
    print ('Count of Facets' + str(count3), file = StatCompTest)
    print ('Median of Facets' + str(median3), file = StatCompTest)
    print ('Std of Facets' + str(std3), file = StatCompTest)
    print ('Var of Facets' + str(var3), file = StatCompTest)

    for f in files2:
        df4 = pd.read_csv(f)
        FacetSizer = pd.DataFrame(df4['relFacetSize'])
        FacetSizerc = FacetSizer.dropna()
        FacetSizeri = FacetSizerc.index
        FacetSizern = len(FacetSizeri)
        for k in range (0, FacetSizern):
            file_list3.append(float(FacetSizerc.iloc[k]))#float!!!


    dfOb = pd.DataFrame(file_list3)

                      
    mean4 = dfOb.mean()
    sum4 = dfOb.sum()
    max4 = dfOb.max()
    min4 = dfOb.min()
    count4 = dfOb.count()
    median4 = dfOb.median() 
    std4 = dfOb.std() 
    var4 = dfOb.var()

    print ('Mean rel Facet Size' + str(mean4), file = StatCompTest)
    print ('Sum of rel Facet Sizes' + str(sum4), file = StatCompTest)
    print ('Max rel Facet Size' + str(max4), file = StatCompTest)
    print ('Min rel Facet Size' + str(min4), file = StatCompTest)
    print ('Count of rel Facet Sizes' + str(count4), file = StatCompTest)
    print ('Median of rel Facet Sizes' + str(median4), file = StatCompTest)
    print ('Std of rel Facet Size' + str(std4), file = StatCompTest)
    print ('Var of rel Facet Size' + str(var4), file = StatCompTest)


#The following code section delivers information on how many neighbous each Label has, according to the algorithm in the octave script
from pandas import DataFrame
dfN1 = pd.read_csv(args.neighbours1, sep="\t", header=None, names=['Label', 'Neighbours'])
dfN2 = pd.read_csv(args.neighbours2, sep="\t", header=None, names=['Label', 'Neighbours'])
dfN3 = pd.read_csv(args.neighbours3, sep="\t", header=None, names=['Label', 'Neighbours'])
dfN4 = pd.read_csv(args.neighbours4, sep="\t", header=None, names=['Label', 'Neighbours'])
dfN5 = pd.read_csv(args.neighbours5, sep="\t", header=None, names=['Label', 'Neighbours'])
dfN6 = pd.read_csv(args.neighbours6, sep="\t", header=None, names=['Label', 'Neighbours'])

dfNList = [dfN1, dfN2, dfN3, dfN4, dfN5, dfN6]

for dfN in dfNList:
    mean5 = dfN['Neighbours'].mean()
    sum5 = dfN['Neighbours'].sum()
    max5 = dfN['Neighbours'].max()
    min5 = dfN['Neighbours'].min()
    count5 = dfN['Neighbours'].count()
    median5 = dfN['Neighbours'].median() 
    std5 = dfN['Neighbours'].std() 
    var5 = dfN['Neighbours'].var()

    print ('Mean Neighbours: ' + str(mean5), file = StatCompTest)
    print ('Sum of Neighbours: ' + str(sum5), file = StatCompTest)
    print ('Max Neighbours: ' + str(max5), file = StatCompTest)
    print ('Min Neighbours: ' + str(min5), file = StatCompTest)
    print ('Count of Neighbours: ' + str(count5), file = StatCompTest)
    print ('Median Neighbours: ' + str(median5), file = StatCompTest)
    print ('Std of Neighbours: ' + str(std5), file = StatCompTest)
    print ('Var of Neighbours: ' + str(var5), file = StatCompTest)


#The next section extracts the volume, surface, elipsoidity and sphericity value od each tsv file in the directory, puts them into a new dataframe (merged_df) to analyze them further

dir1 = args.filess1
dir2 = args.filess2
dir3 = args.filess3
dir4 = args.filess4
dir5 = args.filess5
dir6 = args.filess6

dirList = [dir1, dir2, dir3, dir4, dir5, dir6]
print(dirList)

dfObjList = []

for dir in dirList:

    files = glob.glob(dir)
    print(files)
    file_list4 =[]

    for i in range (1,8):
        for f in files:
            df2 = pd.read_csv(f, sep="\t", names = ['Q', 'U', 'V'])
            Values = pd.DataFrame(df2['V'])
            file_list4.append(float(Values.iloc[i]))
        dfObj = pd.DataFrame(file_list4)
        mean6 = dfObj.mean()
        sum6 = dfObj.sum()
        max6 = dfObj.max()
        min6 = dfObj.min()
        count6 = dfObj.count()
        median6 = dfObj.median() 
        std6 = dfObj.std() 
        var6 = dfObj.var()

        print ('Mean' + str(i) + str(mean6), file = StatCompTest)
        print ('Sum' + str(i) + str(sum6), file = StatCompTest)
        print ('Max' + str(i) + str(max6), file = StatCompTest)
        print ('Min' + str(i) + str(min6), file = StatCompTest)
        print ('Count' + str(i) + str(count6), file = StatCompTest)
        print ('Median' + str(i) + str(median6), file = StatCompTest)
        print ('Std' + str(i) + str(std6), file = StatCompTest)
        print ('Var' + str(i) + str(var6), file = StatCompTest)

        dfObjList.append(dfObj)

        file_list4 = []

        i = i +1



    print('10 = Volume, 20 = d_v, 30 = S_c, 40 = S_o, 50 = S_l/S_o, 60 = Psi, 70 = eta', file = StatCompTest)






StatCompTest.close()

