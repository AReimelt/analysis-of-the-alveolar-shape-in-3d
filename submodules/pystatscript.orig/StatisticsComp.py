#!/usr/bin/env python3
import sys
print(sys.version)
import csv
import glob
import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
import scipy
from scipy import stats


# Code for printing to a file 
StatComp = open('StatComp.txt', 'w')

#The first code section delivers data for the interplanar angles  of the alveoli
df1 = pd.read_csv (r'/home/***REMOVED***/SA-alv_hum_blinded/proc/01/SA/uCTh_ROI01_alv_dmc_sws_o_tFAe.csv')
df2 = pd.read_csv (r'/home/***REMOVED***/SA-alv_hum_blinded/proc/02/SA/uCTh_ROI01_alv_dmc_sws_o_tFAe.csv')
df3 = pd.read_csv (r'/home/***REMOVED***/SA-alv_hum_blinded/proc/03/SA/uCTh_ROI01_alv_dmc_sws_o_tFAe.csv')
df4 = pd.read_csv (r'/home/***REMOVED***/SA-alv_hum_blinded/proc/04/SA/uCTh_ROI01_alv_dmc_sws_o_tFAe.csv')
df5 = pd.read_csv (r'/home/***REMOVED***/SA-alv_hum_blinded/proc/05/SA/uCTh_ROI01_alv_dmc_sws_o_tFAe.csv')
df6 = pd.read_csv (r'/home/***REMOVED***/SA-alv_hum_blinded/proc/06/SA/uCTh_ROI01_alv_dmc_sws_o_tFAe.csv')

dfList = [df1, df2, df3, df4, df5, df6]

for df in dfList:
    mean1 = df['interplanarAngles'].mean()
    sum1 = df['interplanarAngles'].sum()
    max1 = df['interplanarAngles'].max()
    min1 = df['interplanarAngles'].min()
    count1 = df['interplanarAngles'].count()
    median1 = df['interplanarAngles'].median() 
    std1 = df['interplanarAngles'].std() 
    var1 = df['interplanarAngles'].var()
    
    print ('Mean interplanarAngles: '  + str(mean1), file = StatComp)
    print ('Sum of interplanarAngles: ' + str(sum1), file = StatComp)
    print ('Max interplanarAngles: ' + str(max1), file = StatComp)
    print ('Min interplanarAngles: ' + str(min1), file = StatComp)
    print ('Count of interplanarAngles: ' + str(count1), file = StatComp)
    print ('Median interplanarAngles: ' + str(median1), file = StatComp)
    print ('Std of interplanarAngles: ' + str(std1), file = StatComp)
    print ('Var of interplanarAngles: ' + str(var1), file = StatComp)



#The next section has the goal to analyze the relative and absolute facet sizes
dir1 = '/home/***REMOVED***/SA-alv_hum_blinded/proc/01/SA/uCTh_ROI01_alv_dmc_sws_o_a00*FA.csv'
dir2 = '/home/***REMOVED***/SA-alv_hum_blinded/proc/02/SA/uCTh_ROI01_alv_dmc_sws_o_a00*FA.csv'
dir3 = '/home/***REMOVED***/SA-alv_hum_blinded/proc/03/SA/uCTh_ROI01_alv_dmc_sws_o_a00*FA.csv'
dir4 = '/home/***REMOVED***/SA-alv_hum_blinded/proc/04/SA/uCTh_ROI01_alv_dmc_sws_o_a00*FA.csv'
dir5 = '/home/***REMOVED***/SA-alv_hum_blinded/proc/05/SA/uCTh_ROI01_alv_dmc_sws_o_a00*FA.csv'
dir6 = '/home/***REMOVED***/SA-alv_hum_blinded/proc/06/SA/uCTh_ROI01_alv_dmc_sws_o_a00*FA.csv'

dirList = [dir1, dir2, dir3, dir4, dir5, dir6]

for dir in dirList:

    files2 = glob.glob(dir)

    #print(files2)

    file_list = []
    file_list2 = []
    file_list3 = []

    for f in files2:
        df3 = pd.read_csv(f)
        FacetSize = pd.DataFrame(df3['absFacetSize'])
        FacetSizec = FacetSize.dropna()
        FacetSizei = FacetSizec.index
        FacetSizen = len(FacetSizei)
        file_list2.append(FacetSizen)
        for j in range (0, FacetSizen):
            file_list.append(float(FacetSizec.iloc[j]))#float!!!


    dfObj = pd.DataFrame(file_list)

                      
    mean2 = dfObj.mean()
    sum2 = dfObj.sum()
    max2 = dfObj.max()
    min2 = dfObj.min()
    count2 = dfObj.count()
    median2 = dfObj.median() 
    std2 = dfObj.std() 
    var2 = dfObj.var()

    print ('Mean Facet Size' + str(mean2), file = StatComp)
    print ('Sum of Facet Sizes' + str(sum2), file = StatComp)
    print ('Max Facet Size' + str(max2), file = StatComp)
    print ('Min Facet Size' + str(min2), file = StatComp)
    print ('Count of Facet Sizes' + str(count2), file = StatComp)
    print ('Median of Facet Sizes' + str(median2), file = StatComp)
    print ('Std of Facet Size' + str(std2), file = StatComp)
    print ('Var of Facet Size' + str(var2), file = StatComp)

    dfFS = pd.DataFrame(file_list2)

    mean3 = dfFS.mean()
    sum3 = dfFS.sum()
    max3 = dfFS.max()
    min3 = dfFS.min()
    count3 = dfFS.count()
    median3 = dfFS.median() 
    std3 = dfFS.std() 
    var3 = dfFS.var()

    print ('Mean Facets' + str(mean3), file = StatComp)
    print ('Sum of Facets' + str(sum3), file = StatComp)
    print ('Max Facets' + str(max3), file = StatComp)
    print ('Min Facets' + str(min3), file = StatComp)
    print ('Count of Facets' + str(count3), file = StatComp)
    print ('Median of Facets' + str(median3), file = StatComp)
    print ('Std of Facets' + str(std3), file = StatComp)
    print ('Var of Facets' + str(var3), file = StatComp)

    for f in files2:
        df4 = pd.read_csv(f)
        FacetSizer = pd.DataFrame(df4['relFacetSize'])
        FacetSizerc = FacetSizer.dropna()
        FacetSizeri = FacetSizerc.index
        FacetSizern = len(FacetSizeri)
        for k in range (0, FacetSizern):
            file_list3.append(float(FacetSizerc.iloc[k]))#float!!!


    dfOb = pd.DataFrame(file_list3)

                      
    mean4 = dfOb.mean()
    sum4 = dfOb.sum()
    max4 = dfOb.max()
    min4 = dfOb.min()
    count4 = dfOb.count()
    median4 = dfOb.median() 
    std4 = dfOb.std() 
    var4 = dfOb.var()

    print ('Mean rel Facet Size' + str(mean4), file = StatComp)
    print ('Sum of rel Facet Sizes' + str(sum4), file = StatComp)
    print ('Max rel Facet Size' + str(max4), file = StatComp)
    print ('Min rel Facet Size' + str(min4), file = StatComp)
    print ('Count of rel Facet Sizes' + str(count4), file = StatComp)
    print ('Median of rel Facet Sizes' + str(median4), file = StatComp)
    print ('Std of rel Facet Size' + str(std4), file = StatComp)
    print ('Var of rel Facet Size' + str(var4), file = StatComp)


#The following code section delivers information on how many neighbous each Label has, according to the algorithm in the octave script
from pandas import DataFrame
dfN1 = pd.read_csv('~/SA-alv_hum_blinded/proc/01/SA/uCTh_ROI01_alv_dmc_sws_o_faN.tsv', sep="\t", header=None, names=['Label', 'Neighbours'])
dfN2 = pd.read_csv('~/SA-alv_hum_blinded/proc/02/SA/uCTh_ROI01_alv_dmc_sws_o_faN.tsv', sep="\t", header=None, names=['Label', 'Neighbours'])
dfN3 = pd.read_csv('~/SA-alv_hum_blinded/proc/03/SA/uCTh_ROI01_alv_dmc_sws_o_faN.tsv', sep="\t", header=None, names=['Label', 'Neighbours'])
dfN4 = pd.read_csv('~/SA-alv_hum_blinded/proc/04/SA/uCTh_ROI01_alv_dmc_sws_o_faN.tsv', sep="\t", header=None, names=['Label', 'Neighbours'])
dfN5 = pd.read_csv('~/SA-alv_hum_blinded/proc/05/SA/uCTh_ROI01_alv_dmc_sws_o_faN.tsv', sep="\t", header=None, names=['Label', 'Neighbours'])
dfN6 = pd.read_csv('~/SA-alv_hum_blinded/proc/06/SA/uCTh_ROI01_alv_dmc_sws_o_faN.tsv', sep="\t", header=None, names=['Label', 'Neighbours'])

dfNList = [dfN1, dfN2, dfN3, dfN4, dfN5, dfN6]

for dfN in dfNList:
    mean5 = dfN['Neighbours'].mean()
    sum5 = dfN['Neighbours'].sum()
    max5 = dfN['Neighbours'].max()
    min5 = dfN['Neighbours'].min()
    count5 = dfN['Neighbours'].count()
    median5 = dfN['Neighbours'].median() 
    std5 = dfN['Neighbours'].std() 
    var5 = dfN['Neighbours'].var()

    print ('Mean Neighbours: ' + str(mean5), file = StatComp)
    print ('Sum of Neighbours: ' + str(sum5), file = StatComp)
    print ('Max Neighbours: ' + str(max5), file = StatComp)
    print ('Min Neighbours: ' + str(min5), file = StatComp)
    print ('Count of Neighbours: ' + str(count5), file = StatComp)
    print ('Median Neighbours: ' + str(median5), file = StatComp)
    print ('Std of Neighbours: ' + str(std5), file = StatComp)
    print ('Var of Neighbours: ' + str(var5), file = StatComp)


#The next section extracts the volume, surface, elipsoidity and sphericity value od each tsv file in the directory, puts them into a new dataframe (merged_df) to analyze them further
dir1 = '/home/***REMOVED***/SA-alv_hum_blinded/proc/01/SA/uCTh_ROI01_alv_dmc_sws_o_a00*.tsv'
dir2 = '/home/***REMOVED***/SA-alv_hum_blinded/proc/02/SA/uCTh_ROI01_alv_dmc_sws_o_a00*.tsv'
dir3 = '/home/***REMOVED***/SA-alv_hum_blinded/proc/03/SA/uCTh_ROI01_alv_dmc_sws_o_a00*.tsv'
dir4 = '/home/***REMOVED***/SA-alv_hum_blinded/proc/04/SA/uCTh_ROI01_alv_dmc_sws_o_a00*.tsv'
dir5 = '/home/***REMOVED***/SA-alv_hum_blinded/proc/05/SA/uCTh_ROI01_alv_dmc_sws_o_a00*.tsv'
dir6 = '/home/***REMOVED***/SA-alv_hum_blinded/proc/06/SA/uCTh_ROI01_alv_dmc_sws_o_a00*.tsv'

dirList = [dir1, dir2, dir3, dir4, dir5, dir6]

dfObjList = []

for dir in dirList:

    files = glob.glob(dir)
    file_list4 =[]

    for i in range (1,8):
        for f in files:
            df2 = pd.read_csv(f, sep="\t", names = ['Q', 'U', 'V'])
            Values = pd.DataFrame(df2['V'])
            file_list4.append(float(Values.iloc[i]))
        dfObj = pd.DataFrame(file_list4)
        mean6 = dfObj.mean()
        sum6 = dfObj.sum()
        max6 = dfObj.max()
        min6 = dfObj.min()
        count6 = dfObj.count()
        median6 = dfObj.median() 
        std6 = dfObj.std() 
        var6 = dfObj.var()

        print ('Mean' + str(i) + str(mean6), file = StatComp)
        print ('Sum' + str(i) + str(sum6), file = StatComp)
        print ('Max' + str(i) + str(max6), file = StatComp)
        print ('Min' + str(i) + str(min6), file = StatComp)
        print ('Count' + str(i) + str(count6), file = StatComp)
        print ('Median' + str(i) + str(median6), file = StatComp)
        print ('Std' + str(i) + str(std6), file = StatComp)
        print ('Var' + str(i) + str(var6), file = StatComp)

        dfObjList.append(dfObj)

        file_list4 = []

        i = i +1



    print('10 = Volume, 20 = d_v, 30 = S_c, 40 = S_o, 50 = S_l/S_o, 60 = Psi, 70 = eta', file = StatComp)


#print(dfObjList[20])
Comp = scipy.stats.mannwhitneyu(dfObjList[7], dfObjList[7], alternative = None)
print(Comp, file = StatComp)

StatComp.close()




#arraysDict = {}
#for i in range(0,3):
    #arraysDict['x{0}'.format(i)] = [1,2,3]
#arraysList = []
#for i in range(0,3):
    #arraysList.append([1,2,3])
