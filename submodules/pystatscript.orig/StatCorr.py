#!/usr/bin/env python3
import sys
print(sys.version)
import csv
import glob
import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
import scipy
from scipy import stats

with open('Arguments.txt') as f:
    lines = [line.rstrip() for line in f]
#print(lines)

StatCorr = open('StatCorr.txt' , 'w')

#The next section extracts the volume, surface, elipsoidity and sphericity value od each tsv file in the directory, puts them into a new dataframe (merged_df) to analyze them further
file_list4 =[]

for i in range (1,8):
    for l in lines:
        df2 = pd.read_csv(l, sep="\t", names = ['Q', 'U', 'V'])
        Values = pd.DataFrame(df2['V'])
        file_list4.append(float(Values.iloc[i]))
    dfObj02  = pd.DataFrame(file_list4)
    mean6 = dfObj02.mean()
    sum6 = dfObj02.sum()
    max6 = dfObj02.max()
    min6 = dfObj02.min()
    count6 = dfObj02.count()
    median6 = dfObj02.median() 
    std6 = dfObj02.std() 
    var6 = dfObj02.var()

    print ('Mean' + str(i) + str(mean6), file = StatCorr)
    print ('Sum' + str(i) + str(sum6), file = StatCorr)
    print ('Max' + str(i) + str(max6), file = StatCorr)
    print ('Min' + str(i) + str(min6), file = StatCorr)
    print ('Count' + str(i) + str(count6), file = StatCorr)
    print ('Median' + str(i) + str(median6), file = StatCorr)
    print ('Std' + str(i) + str(std6), file = StatCorr)
    print ('Var' + str(i) + str(var6), file = StatCorr)
    

    file_list4 = []

    i = i +1


print('10 = Volume, 20 = d_v, 30 = S_c, 40 = S_o, 50 = S_l/S_o, 60 = Psi, 70 = eta', file = StatCorr)



StatCorr.close()
