#!/usr/bin/env python3
import sys
print(sys.version)
import csv
import glob
import pandas as pd
pd.set_option('display.max_rows', 50000)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 10000)
pd.set_option('max_colwidth' , 1200)
import argparse

tsv_files = argparse.ArgumentParser()
tsv_files.add_argument("-d1", "--directory1")
tsv_files.add_argument("-d2", "--directory2")
tsv_files.add_argument("-d3", "--directory3")
tsv_files.add_argument("-d4", "--directory4")
tsv_files.add_argument("-d5", "--directory5")
tsv_files.add_argument("-d6", "--directory6")
args = tsv_files.parse_args()

#The next section extracts the volume, surface, elipsoidity and sphericity value od each tsv file in the directory, puts them into a new dataframe (merged_df) to analyze them further
dir4 = args.directory1
dir1 = args.directory2
dir2 = args.directory3 
dir3 = args.directory4  
dir5 = args.directory5
dir6 = args.directory6

Exclude2 = open('Exclude2.txt', 'w')

dir_list = [dir4, dir5, dir6]

for dir in dir_list:

    files = glob.glob(dir)

    file_list = []
    file_list4 =[]

    


    for f in files:
        df2 = pd.read_csv(f, sep="\t", names = ['Q', 'U', 'V'])
        Values = pd.DataFrame(df2['V'])
        Include = (float(Values.iloc[6])) + (float(Values.iloc[7])) 
        file_list4.append(float(Include))
        file_list.append(f)
        
    dfObj = pd.DataFrame({'Value' : file_list4, 'filenames' : file_list})
    dfObj05 = pd.DataFrame(file_list4)

    mean6 = dfObj05.mean()
    sum6 = dfObj05.sum()
    max6 = dfObj05.max()
    min6 = dfObj05.min()
    count6 = dfObj05.count()
    median6 = dfObj05.median() 
    std6 = dfObj05.std() 
    var6 = dfObj05.var()
    quant6 = dfObj05.quantile(q = 0.25)

    print ('Mean' + str(mean6), file = Exclude2)
    print ('Sum' + str(sum6), file = Exclude2)
    print ('Max' + str(max6), file = Exclude2)
    print ('Min' + str(min6), file = Exclude2)
    print ('Count' + str(count6), file = Exclude2)
    print ('Median' + str(median6), file = Exclude2)
    print ('Std' + str(std6), file = Exclude2)
    print ('Var' + str(var6), file = Exclude2)
    print ('Quant' + str(quant6), file = Exclude2)

    dfObj.sort_values(by=['Value'], inplace= True)

file_list4 = []





#print(dfObj, file = Exclude)
#print(files)

Exclude2.close()
