#!/usr/bin/env python2
#include "itkImageFileReader.h"
#include "itkLabelImageLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include "itkImageFileWriter.h"


import itk
import sys

if len(sys.argv) != 3:
    print("Usage: " + sys.argv[0] + " <InputFileName> ")
    sys.exit(1)

input_filename =sys.argv[1] 
output_filename = sys.argv[2]

Dimension = 3
PixelType = itk.US
ImageType = itk.Image[PixelType, Dimension]
LabelObjectType = itk.StatisticsLabelObject[itk.UL, Dimension]
LabelMapType = itk.LabelMap[LabelObjectType]

 
reader = itk.ImageFileReader[ImageType].New(FileName=input_filename)
filter = itk.LabelImageToShapeLabelMapFilter[ImageType, LabelMapType].New(reader)
filter.Update()
label_map = filter.GetOutput()

d = 0 #d=diameter of the largest alveolus
for i in range  (1, label_map.GetNumberOfLabelObjects()):
    bounding_box = label_map.GetNthLabelObject(i).GetBoundingBox().GetSize()
    for x in bounding_box:
        if x > d:
            d = x
            
max_range = label_map.GetNumberOfLabelObjects()
RemoveList = []
for i in range  (0, max_range):
    bounding_box_size = label_map.GetNthLabelObject(i).GetBoundingBox().GetSize()
    bounding_box_index = label_map.GetNthLabelObject(i).GetBoundingBox().GetIndex()
    bounding_box_max = bounding_box_size + bounding_box_index
    for y in bounding_box_max:
        if y > 500-d:
            rlabel = label_map.GetNthLabelObject(i)
            RemoveList.append(rlabel)
            break
            
        elif y <d:
            rlabel = label_map.GetNthLabelObject(i)
            RemoveList.append(rlabel)
            break
            

i = len(RemoveList)
j = 0
while j < i:
    label_map.RemoveLabelObject(RemoveList[j])
    #print("Removed " + str(j))
    j += 1
    
        
 
rl_image = itk.LabelMapToLabelImageFilter[LabelMapType, ImageType].New(label_map)

WriterType = itk.ImageFileWriter[ImageType]
writer = WriterType.New()
writer.SetFileName(output_filename)
writer.SetInput(rl_image.GetOutput())
writer.Update()

