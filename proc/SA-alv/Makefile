
### setting default paths of external libraries
ITKLIB?=/opt/itk-4.13.2/lib/cmake/ITK-4.13
VTKLIB?=/opt/vtk-8.1.2_planeIDs4vtkHull/lib/cmake/vtk-8.1/

### setting default paths of external programs
PV?=/opt/ParaView-5.6.0-MPI-Linux-64bit/
GP?=/opt/gnuplot-5.0.6/

## path to submodules
export SUBDIR = $(realpath submodules)

### setting default paths of internal programs for PATH
ITK?=$(SUBDIR)/ITK-CLIs/
VTK?=$(SUBDIR)/VTK-CLIs/
FA?=$(SUBDIR)/FacetAnalyser/

export PATH:= $(ITK)/build:$(PATH)
export PATH:= $(VTK)/build:$(PATH)
export PATH:= $(FA)/build:$(PATH)
export PATH:= $(PV)/bin:$(PATH)
export PATH:= $(GP)/bin:$(PATH)


### list used programs
ITKEXE = hist analyse_labels file_converter
VTKEXE+= mc_discrete smooth-ws threshold vtp2csv vtpFD2csv tf_scale genEll analyse_S+V
## FAEXE for SA
FAEXE = FacetAnalyserCLI
## external programs
EXECUTABLES = parallel
## external programs for SA
EXECUTABLES+= paraview pvpython
EXECUTABLES+= gnuplot
EXECUTABLES+= octave-cli
EXECUTABLES+= $(VGLRUN)
### also check uncommon tools
EXECUTABLES+= datamash pigz parallel
EXECUTABLES+= convert mogrify
## inkscape (without PDF-export bug, e.g. 0.48.5) and inkscape > 0.91
EXECUTABLES+= inkscape inkscape-0.92


SHELL:= /bin/bash

SPACE := $(eval) $(eval)
base_ = $(subst $(SPACE),_,$(filter-out $(lastword $(subst _, ,$1)),$(subst _, ,$1)))
base. = $(subst $(SPACE),.,$(filter-out $(lastword $(subst ., ,$1)),$(subst ., ,$1)))



### check existance of external programs
K:= $(foreach exec,$(EXECUTABLES),\
	$(if $(shell PATH=$(PATH) which $(exec)),some string,$(error "No $(exec) in PATH")))



SUBDIRS:= SA/


.PHONY: all clean $(SUBDIRS)


all : $(SUBDIRS)


clean :
	$(MAKE) -C $(SUBDIRS) clean

## build internal tools
## only build those listed above e.g. ITKEXE
## run with unlimited -j because all involved programms are single threaded, needs spedific rules (intTools.mk) because multiple goals are processed serially ("in turn") even with -j: https://savannah.gnu.org/support/?107274
.PHONY: intTools # make sure intTools is always executed (even if intTools.done already exists)
intTools :
	git submodule update --init --recursive # http://stackoverflow.com/questions/3796927/how-to-git-clone-including-submodules#4438292
	$(MAKE) \
		ITKLIB=$(ITKLIB) ITKEXE='$(ITKEXE)' \
		VTKLIB=$(VTKLIB) VTKEXE='$(VTKEXE)' \
		ITKLIB=$(ITKLIB) FAEXE='$(FAEXE)' \
		-j -f intTools.mk # run with unlimited -j
	INTTOOLS="$(ITKEXE) $(VTKEXE) $(FAEXE)"; \
		PATH=$(PATH); \
		for i in $$INTTOOLS; do if test -z `which $$i`; then echo "Error, No $$i in PATH!" 1>&2; exit 125; fi; done


$(SUBDIRS) : | intTools # order only dep to prevent reexec

$(SUBDIRS) :
	/usr/bin/time -v -o $(dir $@)timing \
	   $(MAKE) -C $(dir $@)

