#!/usr/bin/env python3
import sys
print(sys.version)
import csv
import glob
import pandas as pd
pd.set_option('display.max_rows', 50000)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 10000)
pd.set_option('max_colwidth' , 1200)
import argparse

#The next section extracts the volume, surface, elipsoidity and sphericity value od each tsv file in the directory, puts them into a new dataframe (merged_df) to analyze them further
options = argparse.ArgumentParser()
options.add_argument("-d", "--direct")

args = options.parse_args()
dir = args.direct

files = glob.glob(dir)

file_list = []
file_list4 =[]

Exclude = open('Exclude.txt', 'w')


for f in files:
    df2 = pd.read_csv(f, sep="\t", names = ['Q', 'U', 'V'])
    Values = pd.DataFrame(df2['V'])
    Include = (float(Values.iloc[6])) + (float(Values.iloc[7])) 
    file_list4.append(float(Include))
    file_list.append(f)
dfObj = pd.DataFrame({'Value' : file_list4, 'filenames' : file_list})
dfObj05 = pd.DataFrame(file_list4)

mean6 = dfObj05.mean()
sum6 = dfObj05.sum()
max6 = dfObj05.max()
min6 = dfObj05.min()
count6 = dfObj05.count()
median6 = dfObj05.median() 
std6 = dfObj05.std() 
var6 = dfObj05.var()
quant6 = dfObj05.quantile(q = 0.25)

print ('Mean' + str(mean6), file = Exclude)
print ('Sum' + str(sum6), file = Exclude)
print ('Max' + str(max6), file = Exclude)
print ('Min' + str(min6), file = Exclude)
print ('Count' + str(count6), file = Exclude)
print ('Median' + str(median6), file = Exclude)
print ('Std' + str(std6), file = Exclude)
print ('Var' + str(var6), file = Exclude)
print ('Quant' + str(quant6), file = Exclude)

dfObj.sort_values(by=['Value'], inplace= True) 

dfObj.reset_index(inplace = True, drop = True)


print(dfObj, file = Exclude)
#print(files)

Exclude.close()

Arguments = open ('Arguments.txt', 'w')

for Value in dfObj['Value']:
    if Value > quant6.item():
        x = Value 
        break
    
index = dfObj.loc[dfObj['Value'] == x].index[0]
#print(index, file = Arguments)


dfObjt = dfObj.truncate(before= int(index))

dfObjf = dfObjt['filenames']

dfObjs = dfObjf.to_string(header = False, index = False)

print(dfObjs, file = Arguments)

Arguments.close()
