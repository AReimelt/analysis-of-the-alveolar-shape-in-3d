#!/usr/bin/env python3
import sys
print(sys.version)
import csv
import glob
import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

#The first code section delivers data for the interplanar angles  of the alveoli
df = pd.read_csv (r'/home/***REMOVED***/SA-alv_hum_blinded/proc/06/SA/uCTh_ROI01_alv_dmc_sws_o_tFAe.csv')

mean1 = df['interplanarAngles'].mean()
sum1 = df['interplanarAngles'].sum()
max1 = df['interplanarAngles'].max()
min1 = df['interplanarAngles'].min()
count1 = df['interplanarAngles'].count()
median1 = df['interplanarAngles'].median() 
std1 = df['interplanarAngles'].std() 
var1 = df['interplanarAngles'].var()

# Code for printing to a file 
Stat06_4 = open('Stat06_4.txt', 'w') 

print ('Mean interplanarAngles: ' + str(mean1), file = Stat06_4)
print ('Sum of interplanarAngles: ' + str(sum1), file = Stat06_4)
print ('Max interplanarAngles: ' + str(max1), file = Stat06_4)
print ('Min interplanarAngles: ' + str(min1), file = Stat06_4)
print ('Count of interplanarAngles: ' + str(count1), file = Stat06_4)
print ('Median interplanarAngles: ' + str(median1), file = Stat06_4)
print ('Std of interplanarAngles: ' + str(std1), file = Stat06_4)
print ('Var of interplanarAngles: ' + str(var1), file = Stat06_4)

#The next section has the goal to analyze the relative and absolute facet sizes
dir2 = '/home/***REMOVED***/SA-alv_hum_blinded/proc/06/SA/uCTh_ROI01_alv_dmc_sws_o_a00*FA.csv'

files2 = glob.glob(dir2)

#print(files2)

file_list = []
file_list2 = []
file_list3 = []

for f in files2:
    df6 = pd.read_csv(f)
    FacetSize = pd.DataFrame(df6['absFacetSize'])
    FacetSizec = FacetSize.dropna()
    FacetSizei = FacetSizec.index
    FacetSizen = len(FacetSizei)
    file_list2.append(FacetSizen)
    for j in range (0, FacetSizen):
        file_list.append(float(FacetSizec.iloc[j]))#float!!!


dfFSA06 = pd.DataFrame(file_list)

                      
mean2 = dfFSA06.mean()
sum2 = dfFSA06.sum()
max2 = dfFSA06.max()
min2 = dfFSA06.min()
count2 = dfFSA06.count()
median2 = dfFSA06.median() 
std2 = dfFSA06.std() 
var2 = dfFSA06.var()

print ('Mean Facet Size' + str(mean2), file = Stat06_4)
print ('Sum of Facet Sizes' + str(sum2), file = Stat06_4)
print ('Max Facet Size' + str(max2), file = Stat06_4)
print ('Min Facet Size' + str(min2), file = Stat06_4)
print ('Count of Facet Sizes' + str(count2), file = Stat06_4)
print ('Median of Facet Sizes' + str(median2), file = Stat06_4)
print ('Std of Facet Size' + str(std2), file = Stat06_4)
print ('Var of Facet Size' + str(var2), file = Stat06_4)

dfFSN06 = pd.DataFrame(file_list2)

mean3 = dfFSN06.mean()
sum3 = dfFSN06.sum()
max3 = dfFSN06.max()
min3 = dfFSN06.min()
count3 = dfFSN06.count()
median3 = dfFSN06.median() 
std3 = dfFSN06.std() 
var3 = dfFSN06.var()

print ('Mean Facets' + str(mean3), file = Stat06_4)
print ('Sum of Facets' + str(sum3), file = Stat06_4)
print ('Max Facets' + str(max3), file = Stat06_4)
print ('Min Facets' + str(min3), file = Stat06_4)
print ('Count of Facets' + str(count3), file = Stat06_4)
print ('Median of Facets' + str(median3), file = Stat06_4)
print ('Std of Facets' + str(std3), file = Stat06_4)
print ('Var of Facets' + str(var3), file = Stat06_4)

for f in files2:
    df4 = pd.read_csv(f)
    FacetSizer = pd.DataFrame(df4['relFacetSize'])
    FacetSizerc = FacetSizer.dropna()
    FacetSizeri = FacetSizerc.index
    FacetSizern = len(FacetSizeri)
    for k in range (0, FacetSizern):
        file_list3.append(float(FacetSizerc.iloc[k]))#float!!!


dfFSR06 = pd.DataFrame(file_list3)

                      
mean4 = dfFSR06.mean()
sum4 = dfFSR06.sum()
max4 = dfFSR06.max()
min4 = dfFSR06.min()
count4 = dfFSR06.count()
median4 = dfFSR06.median() 
std4 = dfFSR06.std() 
var4 = dfFSR06.var()

print ('Mean rel Facet Size' + str(mean4), file = Stat06_4)
print ('Sum of rel Facet Sizes' + str(sum4), file = Stat06_4)
print ('Max rel Facet Size' + str(max4), file = Stat06_4)
print ('Min rel Facet Size' + str(min4), file = Stat06_4)
print ('Count of rel Facet Sizes' + str(count4), file = Stat06_4)
print ('Median of rel Facet Sizes' + str(median4), file = Stat06_4)
print ('Std of rel Facet Size' + str(std4), file = Stat06_4)
print ('Var of rel Facet Size' + str(var4), file = Stat06_4)





#The following code section delivers information on how many neighbous each Label has, according to the algorithm in the octave script
from pandas import DataFrame
dfN06 = pd.read_csv('~/SA-alv_hum_blinded/proc/06/SA/uCTh_ROI01_alv_dmc_sws_o_faN.tsv', sep="\t", header=None, names=['Label', 'Neighbours'])

mean5 = dfN06['Neighbours'].mean()
sum5 = dfN06['Neighbours'].sum()
max5 = dfN06['Neighbours'].max()
min5 = dfN06['Neighbours'].min()
count5 = dfN06['Neighbours'].count()
median5 = dfN06['Neighbours'].median() 
std5 = dfN06['Neighbours'].std() 
var5 = dfN06['Neighbours'].var()

print ('Mean Neighbours: ' + str(mean5), file = Stat06_4)
print ('Sum of Neighbours: ' + str(sum5), file = Stat06_4)
print ('Max Neighbours: ' + str(max5), file = Stat06_4)
print ('Min Neighbours: ' + str(min5), file = Stat06_4)
print ('Count of Neighbours: ' + str(count5), file = Stat06_4)
print ('Median Neighbours: ' + str(median5), file = Stat06_4)
print ('Std of Neighbours: ' + str(std5), file = Stat06_4)
print ('Var of Neighbours: ' + str(var5), file = Stat06_4)


#The next section extracts the volume, surface, elipsoidity and sphericity value od each tsv file in the directory, puts them into a new dataframe (merged_df) to analyze them further
dir = '/home/***REMOVED***/SA-alv_hum_blinded/proc/06/SA/uCTh_ROI01_alv_dmc_sws_o_a00*.tsv'

files = glob.glob(dir)

file_list4 =[]



for i in range (1,8):
    for f in files:
        df2 = pd.read_csv(f, sep="\t", names = ['Q', 'U', 'V'])
        Values = pd.DataFrame(df2['V'])
        file_list4.append(float(Values.iloc[i]))
    dfObj06 = pd.DataFrame(file_list4)
    mean6 = dfObj06.mean()
    sum6 = dfObj06.sum()
    max6 = dfObj06.max()
    min6 = dfObj06.min()
    count6 = dfObj06.count()
    median6 = dfObj06.median() 
    std6 = dfObj06.std() 
    var6 = dfObj06.var()

    print ('Mean' + str(i) + str(mean6), file = Stat06_4)
    print ('Sum' + str(i) + str(sum6), file = Stat06_4)
    print ('Max' + str(i) + str(max6), file = Stat06_4)
    print ('Min' + str(i) + str(min6), file = Stat06_4)
    print ('Count' + str(i) + str(count6), file = Stat06_4)
    print ('Median' + str(i) + str(median6), file = Stat06_4)
    print ('Std' + str(i) + str(std6), file = Stat06_4)
    print ('Var' + str(i) + str(var6), file = Stat06_4)

    file_list4 = []

    i = i +1


print('10 = Volume, 20 = d_v, 30 = S_c, 40 = S_o, 50 = S_l/S_o, 60 = Psi, 70 = eta', file = Stat06_4)



Stat06_4.close()
