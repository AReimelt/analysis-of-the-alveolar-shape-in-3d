#!/usr/bin/env python2
#include "itkImageFileReader.h"
#include "itkLabelImageLabelMapFilter.h"
#include "itkRelabelLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"

import itk
import sys

if len(sys.argv) != 3:
    print("Usage: " + sys.argv[0] + " <InputFileName> <OutputFileName>")
    sys.exit(1)

input_filename =sys.argv[1] 
output_filename = sys.argv[2]

Dimension = 3
PixelType = itk.US
ImageType = itk.Image[PixelType, Dimension]
LabelObjectType = itk.StatisticsLabelObject[itk.UL, Dimension]
LabelMapType = itk.LabelMap[LabelObjectType]

reader = itk.ImageFileReader[ImageType].New(FileName=input_filename)
label_map = itk.LabelImageToLabelMapFilter[ImageType, LabelMapType].New(reader)
relabel = itk.RelabelLabelMapFilter[LabelMapType].New(label_map)
rl_image = itk.LabelMapToLabelImageFilter[LabelMapType, ImageType].New(relabel)

WriterType = itk.ImageFileWriter[ImageType]
writer = WriterType.New()
writer.SetFileName(output_filename)
writer.SetInput(rl_image.GetOutput())
writer.Update()

























#input_filename =sys.argv[1] 
#output_filename = sys.argv[2]

#reader = itk.ImageFileReader.New(FileName=input_filename)

#label_map = itk.LabelImageToLabelMapFilter.New(input=readerGet_Output())

#relabel = itk.RelabelLabelMapFilter.New(input=label_mapGetOutput())

#image = itk.LabelMapToLabelImageFilter.New(input=relabelGetOutput())

#image.Update()
#relabeled = image.GetOutput()
