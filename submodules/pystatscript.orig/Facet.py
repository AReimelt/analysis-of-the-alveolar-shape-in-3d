#!/usr/bin/env python3
import sys
import csv
import glob
import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
import scipy
from scipy import stats
from Statistics02 import dfOb02
from Statistics03 import dfOb03





#The next section has the goal to analyze the relative and absolute facet sizes
#dir2 = '/home/***REMOVED***/SA-alv_hum_blinded/proc/02/SA/uCTh_ROI01_alv_dmc_sws_o_a00*FA.csv'

#dir3 = '/home/***REMOVED***/SA-alv_hum_blinded/proc/01/SA/uCTh_ROI01_alv_dmc_sws_o_a00*FA.csv'

#files2 = glob.glob(dir2)
#files3 = glob.glob(dir3)

#print(files)

#file_list = []
#file_list2 = []
#file_list3 = []

#for f in files2:
    #df2 = pd.read_csv(f)
    #FacetSize2 = pd.DataFrame(df2['absFacetSize'])
    #FacetSizec2 = FacetSize2.dropna()
    #FacetSizei2 = FacetSizec2.index
    #FacetSizen2 = len(FacetSizei2)
    #file_list2.append(FacetSizen2)
    #for j in range (0, FacetSizen2):
        #file_list2.append(float(FacetSizec2.iloc[j]))#float!!!

#dfObj2 = pd.DataFrame(file_list2)

#mean1 = dfObj2.mean()
#sum1 = dfObj2.sum()
#max1 = dfObj2.max()
#min1 = dfObj2.min()
#count1 = dfObj2.count()
#median1 = dfObj2.median() 
#std1 = dfObj2.std() 
#var1 = dfObj2.var()

#print ('Mean Facet Size2' + str(mean1))
#print ('Sum of Facet Sizes2' + str(sum1))
#print ('Max Facet Size2' + str(max1))
#print ('Min Facet Size2' + str(min1))
#print ('Count of Facet Sizes2' + str(count1))
#print ('Median of Facet Sizes2' + str(median1))
#print ('Std of Facet Size2' + str(std1))
#print ('Var of Facet Size2' + str(var1))


#for g in files3:
    #df3 = pd.read_csv(g)
    #FacetSize3 = pd.DataFrame(df3['absFacetSize'])
    #FacetSizec3 = FacetSize3.dropna()
    #FacetSizei3 = FacetSizec3.index
    #FacetSizen3 = len(FacetSizei3)
    #file_list2.append(FacetSizen3)
    #for j in range (0, FacetSizen3):
       #file_list3.append(float(FacetSizec3.iloc[j]))#float!!!

#dfObj3 = pd.DataFrame(file_list3)

#mean1 = dfObj3.mean()
#sum1 = dfObj3.sum()
#max1 = dfObj3.max()
#min1 = dfObj3.min()
#count1 = dfObj3.count()
#median1 = dfObj3.median() 
#std1 = dfObj3.std() 
#var1 = dfObj3.var()

#print ('Mean Facet Size3' + str(mean1))
#print ('Sum of Facet Sizes3' + str(sum1))
#print ('Max Facet Size3' + str(max1))
#print ('Min Facet Size3' + str(min1))
#print ('Count of Facet Sizes3' + str(count1))
#print ('Median of Facet Sizes3' + str(median1))
#print ('Std of Facet Size3' + str(std1))
#print ('Var of Facet Size3' + str(var1))


#print(dfObj3)
CompareW = scipy.stats.ttest_ind(dfOb02, dfOb03, axis=0, equal_var=False, nan_policy='propagate')
#CompareMWU = scipy.stats.mannwhitneyu(dfObj2, dfObj3, use_continuity = True, alternative = None)
#CompVar = scipy.stats.levene(df02, df03)

#print(CompVar)

print(CompareW)

#rint(dfObj)

                      
#mean1 = dfObj.mean()
#sum1 = dfObj.sum()
#max1 = dfObj.max()
#min1 = dfObj.min()
#count1 = dfObj.count()
#median1 = dfObj.median() 
#std1 = dfObj.std() 
#var1 = dfObj.var()

#print ('Mean Facet Size' + str(mean1))
#print ('Sum of Facet Sizes' + str(sum1))
#print ('Max Facet Size' + str(max1))
#print ('Min Facet Size' + str(min1))
#print ('Count of Facet Sizes' + str(count1))
#print ('Median of Facet Sizes' + str(median1))
#print ('Std of Facet Size' + str(std1))
#print ('Var of Facet Size' + str(var1))

#dfFS = pd.DataFrame(file_list2)

#mean2 = dfFS.mean()
#sum2 = dfFS.sum()
#max2 = dfFS.max()
#min2 = dfFS.min()
#count2 = dfFS.count()
#median2 = dfFS.median() 
#std2 = dfFS.std() 
#var2 = dfFS.var()

#print ('Mean Facets' + str(mean2))
#print ('Sum of Facets' + str(sum2))
#print ('Max Facets' + str(max2))
#print ('Min Facets' + str(min2))
#print ('Count of Facets' + str(count2))
#print ('Median of Facets' + str(median2))
#print ('Std of Facets' + str(std2))
#print ('Var of Facets' + str(var2))

#for f in files2:
    #df4 = pd.read_csv(f)
    #FacetSizer = pd.DataFrame(df4['relFacetSize'])
    #FacetSizerc = FacetSizer.dropna()
    #FacetSizeri = FacetSizerc.index
    #FacetSizern = len(FacetSizeri)
    #for k in range (0, FacetSizern):
        #file_list3.append(float(FacetSizerc.iloc[k]))#float!!!


#dfOb = pd.DataFrame(file_list3)

                      
#mean1 = dfOb.mean()
#sum1 = dfOb.sum()
#max1 = dfOb.max()
#min1 = dfOb.min()
#count1 = dfOb.count()
#median1 = dfOb.median() 
#std1 = dfOb.std() 
#var1 = dfOb.var()

#print ('Mean rel Facet Size' + str(mean1))
#print ('Sum of rel Facet Sizes' + str(sum1))
#print ('Max rel Facet Size' + str(max1))
#print ('Min rel Facet Size' + str(min1))
#print ('Count of rel Facet Sizes' + str(count1))
#print ('Median of rel Facet Sizes' + str(median1))
#print ('Std of rel Facet Size' + str(std1))
#print ('Var of rel Facet Size' + str(var1))



        
